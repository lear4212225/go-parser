# syntax=docker/dockerfile:1

FROM golang:1.21rc3 as builder

COPY . /go/src/github.com/yaroslavvasilenko/parser/
WORKDIR /go/src/github.com/yaroslavvasilenko/parser/
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o parser ./

FROM alpine:3.17.0 as production

RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/yaroslavvasilenko/parser/ ./
COPY public ./public/
CMD ["./parser"]