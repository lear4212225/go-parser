package main

import (
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /search searchVacancyByQueryRequest
// Search vacancy by query and upload in BD.
// responses:
//   200: searchVacancyByQueryResponse

type search struct {
	Query string `json:"query"`
}

// swagger:parameters searchVacancyByQueryRequest
type searchVacancyByQueryRequest struct {
	// Query for search
	// Required: true
	// In: body
	Query search
}

// swagger:response searchVacancyByQueryResponse
type searchVacancyByQueryResponse struct {
}

// swagger:route POST /delete deleteVacancyByIDRequest
// Delete vacancy by id.
// responses:
//   200: deleteVacancyByIDResponse

type searchForDelete struct {
	ID string `json:"id"`
}

// swagger:parameters deleteVacancyByIDRequest
type deleteVacancyByIDRequest struct {
	// ID for search
	// Required: true
	// In: body
	ID searchForDelete
}

// swagger:response deleteVacancyByIDResponse
type deleteVacancyByIDResponse struct {
}

// swagger:route POST /get getVacancyByIDRequest
// Get vacancy by id.
// responses:
//   200: getVacancyByIDResponse

type searchVacancyID struct {
	ID string `json:"id"`
}

// swagger:parameters getVacancyByIDRequest
type getVacancyByIDRequest struct {
	// ID for search
	// Required: true
	// In: body
	ID searchVacancyID
}

// swagger:response getVacancyByIDResponse
type getVacancyByIDResponse struct {
	// in: body
	Body model.Vacancy
}

// swagger:route POST /list getListVacancyRequest
// Get list vacancy.
// responses:
//   200: getListVacancyResponse

// swagger:parameters getVacancyByIDRequest
type getListVacancyRequest struct {
}

// swagger:response getListVacancyResponse
type getListVacancyResponse struct {
	// in: body
	Body []model.Vacancy
}
