package main

import (
	"gitlab.com/yaroslavvasilenko/selenium/internal/controller"
	"gitlab.com/yaroslavvasilenko/selenium/internal/pkg/config"
	"log"
)

func main() {
	conf, err := config.NewConfig()
	if err != nil {
		log.Println(err)
		return
	}

	err = controller.Run(conf)
	if err != nil {
		log.Println(err)
	}
}
