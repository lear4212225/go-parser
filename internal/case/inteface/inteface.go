package inteface

import (
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
)

type RepoVacancy interface {
	CreateVacancy(vacancy model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]*model.Vacancy, error)
	Delete(id int) error
}

type ServiceVacancy interface {
	CreateVacanciesByID(vac model.Vacancy) error
	GetByID(id int) (model.Vacancy, error)
	GetList() ([]*model.Vacancy, error)
	Delete(id int) error
}
