package service

import (
	"gitlab.com/yaroslavvasilenko/selenium/internal/case/inteface"
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
	"gitlab.com/yaroslavvasilenko/selenium/internal/pkg/config"
	"gitlab.com/yaroslavvasilenko/selenium/internal/repo"
)

func NewVacancyService(cfg *config.Config) (inteface.ServiceVacancy, error) {

	store, err := repo.NewVacancyStorage(cfg)
	if err != nil {
		return nil, err
	}
	ww := &VacancyService{Storage: store}
	return ww, nil

}

type VacancyService struct {
	Storage inteface.RepoVacancy
}

func (s *VacancyService) CreateVacanciesByID(vac model.Vacancy) error {

	return s.Storage.CreateVacancy(vac)
}

func (s *VacancyService) GetByID(id int) (model.Vacancy, error) {
	return s.Storage.GetByID(id)
}
func (s *VacancyService) GetList() ([]*model.Vacancy, error) {
	return s.Storage.GetList()
}
func (s *VacancyService) Delete(id int) error {
	return s.Storage.Delete(id)
}
