package model

type Vacancy struct {
	ID              int     `json:"_id,omitempty" bson:"_id,omitempty" db:"id"`
	Title           string  `json:"title" db:"title"`
	Description     string  `json:"description" db:"description"`
	Company         string  `json:"name" db:"company"`
	LinkToCompany   string  `json:"sameAs" db:"company_url"`
	Logo            string  `json:"logo" db:"company_logo_url"`
	Value           string  `json:"value" db:"value"`
	ValidThrough    string  `json:"validThrough" db:"valid_through"`
	Address         string  `json:"address" db:"address"`
	Currency        string  `json:"currency" db:"salary_currency"`
	MinValue        float64 `json:"minValue" db:"salary_min"`
	MaxValue        float64 `json:"maxValue" db:"salary_max"`
	UnitText        string  `json:"unitText" db:"unit_text"`
	JobLocationType string  `json:"jobLocationType" db:"job_location_type"`
	JobFormat       string  `json:"employmentType" db:"job_format"`
	DatePosted      string  `json:"datePosted" db:"date_posted"`
	Created         string  `json:"created" db:"created_at"`
	Updated         string  `json:"updated" db:"updated_at"`
}
