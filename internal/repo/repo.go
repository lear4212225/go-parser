package repo

import (
	"gitlab.com/yaroslavvasilenko/selenium/internal/case/inteface"
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
	"gitlab.com/yaroslavvasilenko/selenium/internal/pkg/config"
)

func NewVacancyStorage(cfg *config.Config) (inteface.RepoVacancy, error) {
	switch cfg.DB {
	case "postgres":
		return createStoragePostgres(cfg)
	case "mongo":
		return createStorageMongo(cfg)
	}
	return &StorageVacancy{
		vacancyMap: make(map[int]*model.Vacancy, 13),
	}, nil
}
