package repo

import (
	"errors"
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
	"sync"
)

type StorageVacancy struct {
	vacancyMap map[int]*model.Vacancy
	sync.RWMutex
}

func (s *StorageVacancy) CreateVacancy(vacancy model.Vacancy) error {
	s.Lock()
	defer s.Unlock()
	s.vacancyMap[vacancy.ID] = &vacancy
	return nil
}

func (s *StorageVacancy) GetByID(id int) (model.Vacancy, error) {
	elem, ok := s.vacancyMap[id]
	if !ok {
		return model.Vacancy{}, errors.New("vacancy not found")
	}
	return *elem, nil
}

func (s *StorageVacancy) GetList() ([]*model.Vacancy, error) {
	if len(s.vacancyMap) == 0 {
		return nil, errors.New("no vacancies")
	}
	vacancySlice := make([]*model.Vacancy, 0, len(s.vacancyMap))
	for _, elem := range s.vacancyMap {
		vacancySlice = append(vacancySlice, elem)
	}
	return vacancySlice, nil
}

func (s *StorageVacancy) Delete(id int) error {
	_, ok := s.vacancyMap[id]
	if !ok {
		return errors.New("vacancy not found")
	}
	delete(s.vacancyMap, id)
	return nil
}
