package repo

import (
	"context"
	"fmt"
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
	"gitlab.com/yaroslavvasilenko/selenium/internal/pkg/config"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type StorageMongo struct {
	Db *mongo.Collection
}

func (s *StorageMongo) CreateVacancy(vacancy model.Vacancy) error {
	insertResult, err := s.Db.InsertOne(context.TODO(), vacancy)
	if err != nil {
		return err
	}
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
	return nil
}

func (s *StorageMongo) GetByID(id int) (model.Vacancy, error) {
	var result model.Vacancy
	err := s.Db.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&result)
	if err != nil {
		return model.Vacancy{}, err
	}
	fmt.Println("Found a single document: ", result)

	return result, nil
}

func (s *StorageMongo) GetList() ([]*model.Vacancy, error) {
	findOptions := options.Find()
	var users []*model.Vacancy
	cur, err := s.Db.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(context.TODO()) {
		var elem model.Vacancy
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, &elem)
	}

	fmt.Println("list users: ", users)
	return users, err
}

func (s *StorageMongo) UpdateVacancyByID(v *model.Vacancy) error {

	update := bson.M{"$set": bson.M{"description": v.Description}}
	updateResult, err := s.Db.UpdateOne(context.TODO(), bson.M{"id": v.ID}, update)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Matched count: ", updateResult.MatchedCount)
	fmt.Println("Modified count: ", updateResult.ModifiedCount)

	return nil
}

func (s *StorageMongo) Delete(id int) error {
	deleteResult, err := s.Db.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return err
	}
	fmt.Println("Deleted count: ", deleteResult.DeletedCount)
	return nil
}

func createStorageMongo(cfg *config.Config) (*StorageMongo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:27017", cfg.DbHost)))
	if err != nil {
		return nil, err
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, err
	}
	collection := client.Database("test").Collection("trainers")

	return &StorageMongo{Db: collection}, nil

}
