package controller

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"gitlab.com/yaroslavvasilenko/selenium/internal/model"
	"net/http"
	"strconv"
	"strings"
)

const habrDomen = "https://career.habr.com"

func (c *App) CreateVacancy(w http.ResponseWriter, r *http.Request) {
	var err error
	var pet map[string]string
	err = json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	query, ok := pet["query"]
	if !ok {
		http.Error(w, "no find query", http.StatusBadRequest)
		return
	}

	app, err := newSelenium()
	if err != nil {
		http.Error(w, "no connection selesnium", http.StatusBadRequest)
		return
	}
	// после окончания программы завершаем работу драйвера
	defer app.wd.Quit()

	// сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы
	err = app.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	elem, err := app.wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	countVacantRaw, err := elem.Text()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	countVacant, err := strconv.Atoi(strings.Split(countVacantRaw, " ")[1])
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var idSlice []string
	for i := (countVacant / 25) + 1; i != 0; i-- {
		err = app.wd.Get(
			fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, query))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		elems, err := app.wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return
		}
		for i := range elems {
			var link string
			link, err := elems[i].GetAttribute("href")
			if err != nil {
				continue
			}
			idSlice = append(idSlice, link)
		}
	}

	for _, idString := range idSlice {
		resp, err := http.Get(habrDomen + idString)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil && doc != nil {
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			http.Error(w, "no found - \"script[type=\"application/ld+json\"]\"", http.StatusInternalServerError)
			return
		}
		var vac model.Vacancy
		err = json.Unmarshal([]byte(dd.Text()), &vac)
		if dd == nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		id, err := strconv.Atoi(strings.Split(idString, "/")[2])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		vac.ID = id

		err = c.ServiceVacancy.CreateVacanciesByID(vac)
		if err != nil {
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	}

}

func (c *App) GetVacancyByID(w http.ResponseWriter, r *http.Request) {
	var err error
	var pet map[string]int
	err = json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	id, ok := pet["id"]
	if !ok {
		http.Error(w, "no find id", http.StatusBadRequest)
		return
	}
	vac, err := c.ServiceVacancy.GetByID(id)
	if err != nil {

		http.Error(w, err.Error(), http.StatusNotFound)
		return

	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(vac)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (c *App) GetListVacancy(w http.ResponseWriter, r *http.Request) {
	vacancySlice, err := c.ServiceVacancy.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(vacancySlice)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *App) DeleteVacancyByID(w http.ResponseWriter, r *http.Request) {
	var err error
	var pet map[string]int
	err = json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	id, ok := pet["id"]
	if !ok {
		http.Error(w, "no find id", http.StatusBadRequest)
		return
	}
	err = c.ServiceVacancy.Delete(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)

}
