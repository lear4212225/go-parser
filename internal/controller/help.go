package controller

import (
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"log"
)

const maxTries = 5

type driverSelenium struct {
	wd selenium.WebDriver
}

func newSelenium() (*driverSelenium, error) {

	a := driverSelenium{}

	chrCaps := chrome.Capabilities{W3C: true}               // прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{"browserName": "firefox"} // добавляем в конфигурацию драйвера настройки для chrome
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var err error

	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		//  selenium.DefaultURLPrefix
		a.wd, err = selenium.NewRemote(caps, "http://seleniumFirefox:4444/wd/hub")
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}

	return &a, err
}
