package controller

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/yaroslavvasilenko/selenium/internal/case/inteface"
	"gitlab.com/yaroslavvasilenko/selenium/internal/pkg/config"
	"gitlab.com/yaroslavvasilenko/selenium/internal/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type App struct {
	ServiceVacancy inteface.ServiceVacancy
}

func NewApp(cfg *config.Config) (*App, error) {
	serv, err := service.NewVacancyService(cfg)
	if err != nil {
		return nil, err
	}
	return &App{ServiceVacancy: serv}, nil
}

func Run(conf *config.Config) error {
	r, err := newRoute(conf)
	if err != nil {
		return err
	}

	srv := &http.Server{
		Addr:    ":" + conf.Addr,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", conf.Addr))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
	return nil
}

func newRoute(cfg *config.Config) (*chi.Mux, error) {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	//  main controller
	controller, err := NewApp(cfg)
	if err != nil {
		return nil, err
	}
	r.Post("/search", controller.CreateVacancy)
	r.Post("/get", controller.GetVacancyByID)
	r.Get("/list", controller.GetListVacancy)
	r.Post("/delete", controller.DeleteVacancyByID)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	return r, nil
}
